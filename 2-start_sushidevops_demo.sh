#!/bin/bash

cd app

for yaml in sushi-proxy_demo.yaml sushi-dev.yaml sushi-x86.yaml sushi-Z.yaml sushi-power_sandbox.yaml; do
    argocd app create -f $yaml
done
